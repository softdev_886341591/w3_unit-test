/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.SoftDev;

import java.util.Scanner;

/**
 *
 * @author focus
 */
public class Ox {
//ถ้าไม่ได้กำหนดเป็น static จะใช้ไม่ได้ เพราะมันจะถือว่าเป็นของ Object ที่เกิดขึ้น
    static char table[][] = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    static char currentPlayer = 'O';
    static int row, col;
    static Scanner sc = new Scanner(System.in);
    static boolean finish = false;
    static int count = 0;

    public static void main(String[] args) {

        showWelcome();
        while (true) {
            showTable();
            showTurn();
            inputRowCol();
            process();
            if (finish) {
                break;
            }
        }
    }

    //static คือ method นี้เป็นของ class นี้เลย ไม่ต้อง instant 
    public static void showTable() {
        for (int r = 0; r < table.length; r++) {
            for (int c = 0; c < table.length; c++) {
                System.out.print(table[r][c]);
            }
            System.out.println("");
        }
    }

    public static void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    private static void showTurn() {
        System.out.println("Turn " + currentPlayer);
    }

    private static void inputRowCol() {

        System.out.println("Please input row, col");
        row = sc.nextInt();
        col = sc.nextInt();
    }

    private static void process() {
        if (setTable()) {
            if (checkWin(table, currentPlayer, col, row)) {
                finish = true;
                showWin();
                return;
            }
            if(checkDraw(count)){
                finish = true;
                showDraw();
                return;
            }
            count++;
            switchPlayer();
        }
    }

    private static void showWin() {
        showTable();
        System.out.println(">>>" + currentPlayer + " Win<<<");
    }

    public static void switchPlayer() {
        if (currentPlayer == 'O') {
            currentPlayer = 'X'; //switchPlayer()
        } else {
            currentPlayer = 'O';
        }
    }

    public static boolean setTable() {
        table[row - 1][col - 1] = currentPlayer;
        return true;
    }

    public static boolean checkWin(char[][] table, char currentPlayer, int col, int row) {
        if (checkVertical(table, currentPlayer, col)) {
            return true;
        } else if (checkHorizontal(table, currentPlayer, row)) {
            return true;
        } else if (checkCross(table, currentPlayer)) {
            return true;
        }
        return false;
    }

    public static boolean checkVertical(char[][] table, char currentPlayer, int col) {
        for (int r = 0; r < table.length; r++) {
            if (table[r][col - 1] != currentPlayer) { //ถ้ามีทุกอันที่ไม่เท่ากับผู้เล่นปัจจุบันให้return false
                return false;
            }
        }
        return true; //ถ้าในแถวแนวตั้งมีเท่าผู้เล่นปัจจุบันให้ return true
    }

    public static boolean checkHorizontal(char[][] table, char currentPlayer, int row) {
        for (int c = 0; c < table.length; c++) {
            if (table[row - 1][c] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    public static boolean checkCross(char[][] table, char currentPlayer) {
        if (checkX1(table, currentPlayer)) {
            return true;
        } else if (checkX2(table, currentPlayer)) {
            return true;
        }
        return false;
    }

    public static boolean checkX1(char[][] table, char currentPlayer) { // 1 1, 2 2, 3 3
        for (int i = 0; i < table.length; i++) {
            if (table[i][i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    public static boolean checkX2(char[][] table, char currentPlayer) {// 1 3, 2 2, 3 1
        for (int i = 0; i < table.length; i++) {
            if (table[i][2-i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    public static boolean checkDraw(int count) {
        if(count == 8){
            return true;
        }
        return false;
    }

    public static void showDraw() {
        System.out.println(">>>Draw<<<");
    }
}

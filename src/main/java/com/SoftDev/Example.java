/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.SoftDev;

import java.util.Scanner;

/**
 *
 * @author focus
 */
class Example {

    static int add(int a, int b) {
        return a + b;
    }

    static String chup(char player1, char player2) {
        if (player1 == 's' && player2 == 'p') {
            return "p1";
        } else if (player1 == 'h' && player2 == 's') {
            return "p1";
        } else if (player1 == 'p' && player2 == 'h') {
            return "p1";
        } else if (player1 == 'h' && player2 == 'p') {
            return "p2";
        } else if (player1 == 'p' && player2 == 's') {
            return "p2";
        } else if (player1 == 's' && player2 == 'h') {
            return "p2";
        }
        return "draw";
    }

    public static void main(String[] args) {
        System.out.println("Start");
        System.out.print("Please input player 1 (p, h, s) : ");
        Scanner sc = new Scanner(System.in);
        String player1 = sc.next();
        System.out.print("Please input player 2  (p, h, s) : ");
        String player2 = sc.next();
        String winner = chup(player1.charAt(0), player2.charAt(0));
        System.out.println("Winner is : " + winner);
    }//Finish
}
